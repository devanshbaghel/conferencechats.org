from datetime import datetime, timedelta
from pathlib import Path
from textwrap import dedent

import pytz
from icalendar import Calendar, Event, vCalAddress, vText
from render_engine import Collection, Page, Site
from render_engine.blog import Blog
from render_engine.feeds import RSSFeed
from render_engine.parsers.markdown import MarkdownPageParser
from render_engine.plugins import CleanOutput
from render_engine_tailwindcss import TailwindCSS
from textunwrap import unwrap

from plugins.render_engine_favicons import Favicons, make_favicons_filter

BASEDIR = Path(__file__).parent
OUTPUT_DIR = BASEDIR.parent / "public"


class Site(Site):
    output_path = OUTPUT_DIR
    static_path = "static"

    site_vars = {
        "SITE_TITLE": "Conference Chats",
        "SITE_URL": "https://conferencechats.org",
        "DISCORD_INVITE_URL": "https://discord.gg/x5KCrCyggN",
        "REPOSITORY_URL": "https://gitlab.com/jonafato/conferencechats.org",
        "DATETIME_FORMAT": "%Y-%m-%d",
    }

    plugins = [
        CleanOutput,
        TailwindCSS,
        Favicons,
    ]


site = Site()

site.engine.globals["favicons"] = make_favicons_filter(site)


class ICSPage(Page):
    template = "verbatim.txt"
    extension = ".ics"
    output_path = Path(".")
    ics_description = unwrap(
        dedent(
            f"""
        Join each month to discuss news, tips and tricks, and other topics related to
        conference organizing. No experience is necessary, only an interest in running
        events; whether you're an experienced conference organizer or curious about
        what goes into running your first event, you're welcome here. Join via Discord
        at {site.site_vars['DISCORD_INVITE_URL']}. Happy organizing!
        """
        )
    )
    exdates = []

    @property
    def _content(self):
        calendar = Calendar()
        calendar.add("prodid", "-//conferencechats.org//EN")
        calendar.add("version", "2.0")
        event = Event()
        event.add("uid", self.uid)
        event.add("summary", "Monthly conference organizers' call")
        event.add("description", self.ics_description)
        event.add("dtstart", self.dtstart)
        event["dtstart"].params["tzid"] = "America/New_York"
        event.add("dtstamp", datetime(2023, 1, 1, 0, 0))
        event["dtstamp"].params["tzid"] = "America/New_York"
        event.add("duration", timedelta(hours=2))
        event.add(
            "rrule",
            {
                "freq": "monthly",
                "byday": self.byday,
            },
        )
        if self.exdates:
            event.add('exdate', self.exdates)
        organizer = vCalAddress("MAILTO:jon@conferencechats.org")
        organizer.params["cn"] = vText("Jon Banafato")
        organizer.params["ROLE"] = vText("Host")
        event.add("organizer", organizer)
        calendar.add_component(event)
        return calendar.to_ical().decode("utf-8").replace("\r\n", "\n").strip()


class Feed(RSSFeed):
    extension = ".xml"


@site.collection
class News(Blog):
    PageParser = MarkdownPageParser
    content_path = "news"
    template = "news_item.html"
    archive_template = "news_list.html"
    Feed = Feed
    routes = ["news"]


@site.collection
class Pages(Collection):
    PageParser = MarkdownPageParser
    template = "base.html"
    content_path = BASEDIR / "pages"
    output_path = "."


@site.page
class Index(Page):
    Parser = MarkdownPageParser
    template = "index.html"
    content_path = BASEDIR / "index/index.md"
    output_path = Path(".")


@site.page
class FirstFridayICS(ICSPage):
    byday = "+1fr"
    dtstart = datetime(2023, 1, 6, 13, 0, 0)
    exdates = [
        datetime(2023, 10, 6, 13, 0, 0),
    ]
    slug = "first-friday"
    # Generated with uuid.uuid4 per https://icalendar.org/New-Properties-for-iCalendar-RFC-7986/5-3-uid-property.html
    uid = "8a96ca6a-f44f-41bf-9e3e-ffc7408620cb"


@site.page
class SecondWednesdayICS(ICSPage):
    byday = "+2we"
    dtstart = datetime(2023, 1, 11, 21, 0, 0)
    slug = "second-wednesday"
    # Generated with uuid.uuid4 per https://icalendar.org/New-Properties-for-iCalendar-RFC-7986/5-3-uid-property.html
    uid = "7420cdcb-6af9-44fc-aa49-00b13c25e126"
