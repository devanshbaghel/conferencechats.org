---
title: Code of Conduct
slug: code-of-conduct
---
## Standards

The Conference Organizers Open Chats community is dedicated to providing a
harassment-free experience for everyone. We do not tolerate harassment of
community members in any form. Participants violating these rules may be
sanctioned or expelled from community spaces at the discretion of the
organizers.

## Inappropriate behavior

Harassment includes:

- offensive verbal comments related to gender, gender identity and expression,
  sexual orientation, disability, physical appearance, body size, race, religion
- NSFW ("not safe for work") or sexual images in community spaces
- deliberate intimidation, stalking, following, harassing photography or
  recording
- sustained disruption of discussions
- inappropriate contact
- unwelcome sexual attention

## Consequences

Participants asked to stop any harassing behavior are expected to comply
immediately. If a participant engages in harassing behavior, the organizers may
take any action they deem appropriate, including warning the offender or
expulsion from community spaces.  In the event of serious infractions of the
code of conduct, organizers may share incident details and offenders' names with
third parties including but not limited to: chat platform staff, law
enforcement, and organizers of other communities. If you are being harassed,
notice that someone else is being harassed, or have any other concerns, please
contact a member of the organizing team immediately. Organizers can be
identified by user roles assigned in Discord.

## Scope

This code of conduct applies to all community spaces, including but not limited
to Discord text chat and scheduled audio / video calls.


## Contact Information

The following community members are Code of Conduct responders:

- Jon Banafato <jon@conferencechats.org>

They can be reached collectively at <conduct@conferencechats.org> or
individually using the email addresses listed above.

We expect participants to follow these rules in all community spaces.

## Attribution

This Code of Conduct was adapted from the
[PyGotham Code of Conduct](https://conduct.pygotham.org) with inspiration from
the [Python Community Code of Conduct](https://www.python.org/psf/conduct/).
