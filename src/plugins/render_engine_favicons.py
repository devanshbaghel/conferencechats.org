from pathlib import Path

from favicons import Favicons as _Favicons
from jinja2 import pass_environment
from render_engine.hookspecs import hook_impl


def make_favicons_filter(site):
    def favicons_filter():
        static_path = Path(site.static_path) / "images" / "logo.png"
        with _Favicons(static_path, site.output_path) as favicons:
            return favicons.html()

    return favicons_filter


class Favicons:
    @hook_impl
    def post_build_site(site):
        static_path = Path(site.static_path) / "images" / "logo.png"
        with _Favicons(static_path, site.output_path) as favicons:
            favicons.generate()
