---
date: 2023-06-15 00:00:00 -04:00
title: July is New Organizers Month
---

Conference Chats video calls will be focused on Q&A for new conference organizers for
the month of July. Our calls are typically open to whatever attendees bring to
the table (rather than prescribing topics), but we see some common questions from
people planning their first conference. With these themed sessions, we hope to
answer questions for a wider audience, some of which you may not even know you
have. If you're exploring the idea of starting a conference, have recently
started a new one, or have inherited an existing one, we encourage you to join!

<!-- snippet -->

The group is full of regular attendees with many decades of combined organizing
experience, and we'd be happy to help you fill in gaps and jump start your conference
planning experience. If you've ever wondered things like:

- What does it cost to put on the conference I want to run?
- What should my timeline look like?
- What types of things can my conference venue provide to me? Wait, they _require_ that
  I purchase those things through them? Soda costs _how much_?

Then this is the group for you. We're a supportive group, and attendance is free; all
we ask is that you share what you learn from your organizing experience with future
conference organizers so that we can all benefit.

There are links to ICS files on the [home page](/) to add the upcoming events to your
calendar. If you'd like to join, you can find us on
[Discord](https://discord.gg/x5KCrCyggN). Happy organizing!
