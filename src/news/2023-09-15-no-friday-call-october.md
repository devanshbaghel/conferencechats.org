---
date: 2023-09-15 00:00:00 -04:00
title: No Friday Call in October 2023
---

October 2023's Friday call is cancelled due to a scheduling conflict (gotta run
a conference!). Please join October's second Wednesday call or November's Friday
call to discuss any and everything related to conference organizing with the
Conference Chats community.