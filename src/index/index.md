---
title: Conference Chats
slug: index
---

## About

This is a community of conference organizers that meet monthly to share advice,
support, news, and other topics related to event planning. It began as an online
extension of the kinds of gatherings that naturally happen among organizers when
attending events together and has been meeting monthly since late 2019. No
experience is necessary to join, only an interest in building communities.
Participants' experience ranges from brand new to event organizing through many
years of running events for thousands of attendees.

Whether you've been running events for years or just wondering what goes into
making your favorite conferences happen, this group is for you. To join, review
our [Code of Conduct](/code-of-conduct.html) and then meet us in [Discord](https://discord.gg/x5KCrCyggN).

## FAQ

### When and where does the group meet?

Video calls take place in Discord at the following times:

- the first Friday of the month starting at 1:00PM US Eastern time ([ICS
  file](/first-friday.ics))
- the second Wednesday of the month starting at 9:00PM US Eastern time ([ICS
  file](/second-wednesday.ics))

See the calendar below to view these events in your local time zone.

These calls typically last several hours, and participants are free to drop in any time
during a scheduled event. Outside of these video calls, we share news and discussion
throughout the month via text chat.

<div id="calendar"></div>


### I've never run a conference before. Can I still participate?

Of course! The experienced organizers of the group are happy to share what they
know. Talking with other organizers is a great way to jump start your first
event.

### What kinds of conferences do we talk about?

Primarily annual community-run conferences, but there are no strict rules about
this. For historical and network effect reasons, we cover a lot of Python and
related open source technology events. Events planning is the common thread,
though; topics need not be specific to the tech sector.

### I have more questions!

Join us in Discord, and we'll be happy to answer them.
